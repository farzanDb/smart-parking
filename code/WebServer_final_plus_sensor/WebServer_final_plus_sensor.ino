/*
  Web Server

  A simple web server that shows the value of the analog input pins.
  using an Arduino Wiznet Ethernet shield.

  Circuit:
   Ethernet shield attached to pins 10, 11, 12, 13
   Analog inputs attached to pins A0 through A5 (optional)

  created 18 Dec 2009
  by David A. Mellis
  modified 9 Apr 2012
  by Tom Igoe
  modified 02 Sept 2015
  by Arturo Guadalupi

*/

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFF, 0xED
};
int capacity = 123;
int val_right = 0;
int val_left = 0;
int dis_right = 0;
int dis_left = 0;
int dis = 0;
int led_left = 0;
int led_right = 0;
int flag = 0;
enum { no_on, right_on, left_on , both_on} state = no_on;
enum {right2left, left2right , no2no} Direction = no2no;

IPAddress ip(169, 254, 151, 139);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  pinMode(7, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);  pinMode(7, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}


void loop() {
  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 1");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin

          client.print(capacity);

          client.println("<br />");

          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }


  //sensor part////////////////////////////////////////////////////////////////
  val_right = analogRead(0);
  val_left = analogRead(1);
  dis_right = 28250 / (val_right - 129.5);
  dis_left =  28250 / (val_left - 129.5);
  delay(100);




  if (dis_left < 100) //car is in front of us
  {
    led_left = 1;
    digitalWrite(5, HIGH);
  }
  else {
    led_left = 0;
    digitalWrite(5, LOW);
  }

  if (dis_right < 100) //car is in front of us
  {
    led_right = 1;
    digitalWrite(7, HIGH);
  }
  else {
    led_right = 0;
    digitalWrite(7, LOW);
  }





  if (led_right == 1 && led_left == 0 && state == no_on) {
    state = right_on;
    Direction = right2left;
    Serial.println("right sensor senses obstacle");
  }
  else if (led_left == 1 && led_right == 0 && state == no_on) {
    state = left_on;
    Direction = left2right;
    Serial.println("left sensor senses obstacle");
  }
  //  else if (led_right == 1 && led_left== 0 && state==no_on){
  //    state= right_on;
  //    Direction= right2left;
  //    Serial.println("right sensor senses obstacle");
  //  }
  //  else if (led_left == 1 && led_right== 0 && state==no_on){
  //    state= left_on;
  //    Direction=left2right;
  //    Serial.println("left sensor senses obstacle");
  //  }





  else if (led_right == 1 && led_left == 0 && state == left_on) {
    state = right_on;
    Direction = right2left;
    Serial.println("right sensor senses obstacle");
  }
  else if (led_left == 1 && led_right == 0 && state == right_on) {
    state = left_on;
    Direction = left2right;
    Serial.println("left sensor senses obstacle");
  }




  //car is in front
  else if (led_left == 1 && led_right == 1 && (state == right_on || state == left_on)) {
    state = both_on;
    Serial.println("car is in front of the sensor");
  }




  else if ( led_right == 0 && led_left == 0 && state == both_on) {
    if (Direction == left2right) {
      Serial.println("   a car passed from left to right");
      state = no_on;
      Direction = no2no;
      capacity = capacity + 1;
    }
    else if (Direction == right2left ) {
      Serial.println("   a car passed from right to left");
      state = no_on;
      Direction = no2no;
      capacity = capacity - 1;
    }
  }
}

